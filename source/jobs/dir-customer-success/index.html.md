---
layout: job_page
title: "Director Customer Success"
---

As the Director of Customer Success, you will lead our dynamic customer success
team and help drive various customer success initiatives, projects and
strategies.

The right candidate will play a critical role in the delivery of a world-class
service experience to GitLab's customers and channel partners.

The right candidate is responsible for architecting the post-sales customer
experience and lifecycle by building a world class service organization,
implementing technology and processes, and partnering with the sales, product,
engineering and operations teams to deliver the best possible customer
experience.

The right candidate is truly passionate about customer advocacy and has a proven track record to talk about! This is an exciting opportunity to unequivocally influence our customers and furthermore, directly impact GitLab’s overall success.

## Responsibilities:

* Set the overall vision and strategic plan for the Customer Success organization - build and scale an integrated Customer Success organization, with specific focus on Health, Usage and Adoption metrics.
* Drive Customer Success Outcomes: Expand our revenue in accounts through up-sell opportunities with our EE Options, influence future lifetime value through higher product adoption, customer satisfaction and overall health scores, reduce churn and drive new business growth through greater advocacy and reference ability
* Define and Optimize Customer Lifecycle: define segmentation of customer base and varying strategies, identify opportunities for continuous improvement.
* Measure effectiveness of Customer success: define operational metrics for team, establish system for tracking metrics, create cadence for review within the team
* Lead World-Class Customer Success Team: recruit experienced leaders for each functional role, attract high potential individual contributors into the team, create rapid onboarding process for new team members, and foster collaboration within team and across customers
* Deliver transformational leadership and so that the CS team is highly motivated and engaged.  Be an inspirational role model by challenging and maximizing the strength of the team and aligning their efforts to the mission and vision of the organization
* Enhance Effectiveness and Efficiency through technology
* Serve as the lead spokesperson and ambassador for CS, representing the organization at the highest levels to prospective partners and customers. Empower the organization through effective communication
* Align with Sales across cross-sell and up-sell and focus on selling with a retention focus
* Align with Finance around measurement and forecasting with driving profitable gross margin focus

## Requirements for applicant

* 7 - 10 years of management experience – including at least three years heading a successful customer success or services organization in an enterprise software environment
* Strong understanding and knowledge of the customer success role in successful cloud and enterprise environments
* Successful and inspired leadership of a management team
* Experience with customer success and relationship management of large strategic clients. Must be able to oversee the management of a high volume of accounts at all levels: SMB; Mid Market; Enterprise and Partners
* Experience successfully working with senior (C-level) executives
* Demonstrated ability to lead managers and successfully manage global, distributed teams across cultures, lines of business, and geographies
* Possesses a strong management presence and leadership ability, with communication and interpersonal skills that inspire and motivate leaders and teams
* Demonstrated excellence in analytical thinking, problem solving, communication, delegation, planning & organization and judgment
* Able to be flexible and agile in responding to evolving business priorities and dealing with ambiguity
* Able to collaborate across organization and with external stakeholders
* Holds strong operational skills that will drive organizational efficiencies and customer satisfaction
* Willing and able to address escalated client issues with speed and urgency
* You share our [values](/handbook/#values), and work in accordance with those values.
